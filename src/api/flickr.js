const express = require('express');
const Flickr = require('flickr-sdk');

let flickr;
let oauth;
const authTokens = {};
let userID = '';

const setUpOAuth = async () => {
  try {
    oauth = new Flickr.OAuth(
      process.env.FLICKR_CONSUMER_KEY,
      process.env.FLICKR_CONSUMER_SECRET
    );
    const reqOauth = await oauth.request(
      'http://localhost:5000/api/v1/flickr/oauth/callback'
    );
    // console.log("yay!", reqOauth.body);

    authTokens.OAUTH_TOKEN = reqOauth.body.oauth_token;
    authTokens.OAUTH_TOKEN_SECRET = reqOauth.body.oauth_token_secret;

    const authLink = await oauth.authorizeUrl(authTokens.OAUTH_TOKEN);
    console.log('Click on this authorization link as: ', authLink);
  } catch (error) {
    console.log(error);
  }
};

const getNsid = async () => {
  try {
    const data = await flickr.prefs.getContentType();
    console.log(data.body);
    userID = data.body.person.nsid;
  } catch (error) {
    console.error(error);
  }
};

if (!process.env.FLICKR_OAUTH_TOKEN_SECRET) {
  setUpOAuth();
} else {
  flickr = new Flickr(
    Flickr.OAuth.createPlugin(
      process.env.FLICKR_CONSUMER_KEY,
      process.env.FLICKR_CONSUMER_SECRET,
      process.env.FLICKR_OAUTH_TOKEN,
      process.env.FLICKR_OAUTH_TOKEN_SECRET
    )
  );
  getNsid();
}
const router = express.Router();

router.get('/oauth/callback', async (req, res, next) => {
  try {
    const verifiedData = await oauth.verify(
      authTokens.OAUTH_TOKEN,
      req.query.oauth_verifier,
      authTokens.OAUTH_TOKEN_SECRET
    );
    console.log(
      'Add values of oauth_token as FLICKR_OAUTH_TOKEN and oauth_token_secret as FLICKR_OAUTH_TOKEN_SECRET to your `.env` file: ',
      verifiedData.body
    );
    userID = verifiedData.body.user_nsid;
    authTokens.OAUTH_TOKEN = verifiedData.body.oauth_token;
    authTokens.OAUTH_TOKEN_SECRET = verifiedData.body.oauth_token_secret;

    res.json({ message: 'OK' });
  } catch (error) {
    next(error);
  }
});

router.get('/', (req, res) => {
  res.json({ message: 'welcome to flickr api' });
});

router.get('/test', async (req, res, next) => {
  try {
    const data = await flickr.photos.getInfo({
      photo_id: 25825763, // sorry, @dokas
    });
    res.json(data);
  } catch (error) {
    next(error);
  }
});

// Get one photo info
router.get('/photo/:id', async (req, res, next) => {
  try {
    const { id } = req.params;
    const data = await flickr.photos.getInfo({ photo_id: id });
    res.json(data.body);
  } catch (error) {
    next(error);
  }
});

// Get one photo info
router.get('/photosizes/:id', async (req, res, next) => {
  try {
    const { id } = req.params;
    const data = await flickr.photos.getSizes({ photo_id: id });
    res.json(data.body);
  } catch (error) {
    next(error);
  }
});

// Get list of all galleries
router.get('/galleries', async (req, res, next) => {
  try {
    const data = await flickr.galleries.getList({ user_id: userID });
    // let data = await flickr.galleries.getList();
    res.json(data.body);
  } catch (error) {
    next(error);
  }
});

// Get photos in one photoset
router.get('/collections/:id', async (req, res, next) => {
  try {
    const { id } = req.params;
    const data = await flickr.collections.getInfo({
      user_id: userID,
      collection_id: id,
    });
    res.json(data.body);
  } catch (error) {
    next(error);
  }
});

// Get list of all collections
router.get('/collections', async (req, res, next) => {
  try {
    const data = await flickr.collections.getTree({ user_id: userID });
    res.json(data.body);
  } catch (error) {
    next(error);
  }
});

// Get photos in one photoset
router.get('/photosets/:id', async (req, res, next) => {
  try {
    const { id } = req.params;
    const data = await flickr.photosets.getPhotos({
      user_id: userID,
      photoset_id: id,
    });
    res.json(data.body);
  } catch (error) {
    next(error);
  }
});

// Get list of all photosets
router.get('/photosets', async (req, res, next) => {
  try {
    const data = await flickr.photosets.getList({ user_id: userID });
    res.json(data.body);
  } catch (error) {
    next(error);
  }
});

module.exports = router;
