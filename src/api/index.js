const express = require('express');

const emojis = require('./emojis');
const flickr = require('./flickr');

const router = express.Router();

router.get('/', (req, res) => {
  res.json({
    message: 'API - 👋🌎🌍🌏',
  });
});

router.use('/emojis', emojis);
router.use('/flickr', flickr);

module.exports = router;
