# Express API Starter
Created using npx script https://www.npmjs.com/package/create-express-api

## Description
Local server to connect to your own flickr-api. Requires registration on Flickr API https://www.flickr.com/services/apps/create/apply/

## Setup

```
npm install
```

## Lint

```
npm run lint
```

## Test

```
npm run test
```

## Development

```
npm run dev
```
